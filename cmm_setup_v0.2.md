=====

Clean CentOS 7, updates, foundational tuning, then centminmod install

set up ssh, disable password login, create less-privileged user, yum update,
install screen

 

Kernel & blk-mq
---------------

install latest mainline kernel from elrepo

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
setenforce 0
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
screen
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
rpm --import https://www.elrepo.org/RPM-GPG-KEY-elrepo.org
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
rpm -Uvh http://www.elrepo.org/elrepo-release-7.0-3.el7.elrepo.noarch.rpm
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
yum --enablerepo=elrepo-kernel --disableexcludes=all install kernel-ml
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
awk -F\' /^menuentry/{print\$2} /etc/grub2.cfg
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

`4.18.6-1.el7.elrepo.x86_64`

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
grub2-set-default 4.18.6-1.el7.elrepo.x86_64
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
cat /boot/grub2/grubenv |grep saved
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
vi /etc/default/grub
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

change

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
GRUB_DEFAULT=0
GRUB_CMDLINE_LINUX="scsi_mod.use_blk_mq=y dm_mod.use_blk_mq=y"
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

then

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
grub2-mkconfig -o /boot/grub2/grub.cfg
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

stop then start VM from host dash, then check running kernel and available
schedulers

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
uname -r && cat /sys/block/vda/queue/scheduler
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

if mq-deadline and kyber not shown, try modprobe (eg. modprobe kyber-iosched )

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
modprobe mq-deadline
modprobe kyber-iosched
cat /sys/block/vda/queue/scheduler
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

after mq-deadline and kyber shown as available schedulers, then make it stick

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
cat << EOF | tee /etc/modules-load.d/mqdeadline.conf
mq-deadline
EOF
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
cat << EOF | tee /etc/modules-load.d/kyber.conf
kyber-iosched
EOF
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

 

make systemd services

for rotational awareness

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
cat << EOF | tee /etc/systemd/system/rotational.service
[Unit]
Description=VDA rotational awareness

[Service]
Type=oneshot
ExecStart=/bin/bash -c 'echo 0 | tee /sys/block/vda/queue/rotational'

[Install]
WantedBy=multi-user.target
EOF
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

for mq-deadline

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
cat << EOF | tee /etc/systemd/system/mqdeadline.service
[Unit]
Description=VDA mq-deadline scheduler

[Service]
Type=oneshot
ExecStart=/bin/bash -c 'echo mq-deadline | tee /sys/block/vda/queue/scheduler'

[Install]
WantedBy=multi-user.target
EOF
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

for other custom server tuning

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
cat << EOF | tee /etc/systemd/system/addrandom.service  
[Unit]  
Description=no entropy from disks (use havegd instead)
  
[Service]  
Type=oneshot  
ExecStart=/bin/bash -c 'echo 0 | tee /sys/block/vda/queue/add_random'  
  
[Install]  
WantedBy=multi-user.target  
EOF
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

(maybe later make for similar for: ExecStart=/bin/bash -c 'echo 0 \| tee
/sys/block/vda/queue/rq_affinity' )

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
systemctl daemon-reload && systemctl enable rotational.service && systemctl enable mqdeadline.service && systemctl enable addrandom.service
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

reboot

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
systemctl status systemd-modules-load.service && cat /sys/block/vda/queue/scheduler /sys/block/vda/queue/rotational /sys/block/vda/queue/add_random /sys/block/vda/queue/rq_affinity
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

extra checks for blk-mq

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
cat /sys/block/vda/queue/scheduler /sys/module/scsi_mod/parameters/use_blk_mq /sys/module/dm_mod/parameters/use_blk_mq /sys/block/dm-0/dm/use_blk_mq
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

 

Devtoolset 7
------------

set up SCL repo

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
yum install centos-release-scl
rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-SIG-SCLo
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
yum install devtoolset-7
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
scl enable devtoolset-7 bash
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

observe new gcc

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
gcc --version && ld --version
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

 

Other Server Setup
------------------

set up custom profile for tuned-adm
[[this](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/performance_tuning_guide/sect-red_hat_enterprise_linux-performance_tuning_guide-performance_monitoring_tools-tuned_and_tuned_adm)]

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
tuned-adm profile latency-performance
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

 

if custom DNS nameservers are needed, then

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
vi /etc/sysconfig/network-scripts/ifcfg-eth0
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

append values for DNS1 and DNS2 so that file looks like

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
DEVICE=eth0
BOOTPROTO=dhcp
ONBOOT=yes
DNS1=8.8.8.8
DNS2=8.8.4.4
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

restart the interface

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
ifdown eth0
ifup eth0
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

 

set up bbr

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
sysctl net.ipv4.tcp_available_congestion_control
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
modprobe tcp_bbr
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
cat << EOF | tee /etc/modules-load.d/tcpbbr.conf
tcp_bbr
EOF
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

sysctl.d for bbr

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
cat << EOF | tee /etc/sysctl.d/77-tcpbbr.conf
net.core.default_qdisc=fq
net.ipv4.tcp_congestion_control=bbr
EOF
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
sysctl -p
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

reboot

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
sysctl net.ipv4.tcp_available_congestion_control
sysctl -n net.ipv4.tcp_congestion_control
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

 

system entropy
[[link](https://www.certdepot.net/rhel7-get-started-random-number-generator/),[link](https://wiki.archlinux.org/index.php/Haveged#Virtual_machines)]
- Note: Centminmod seems to install haveged by default ;)

set up EPEL

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
yum --enablerepo=extras install epel-release
rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-EPEL-7
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
yum install haveged
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
chkconfig haveged on
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

 

 

Centmin Mod
-----------

enable devtoolset 7

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
scl enable devtoolset-7 bash
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

temporarily set SELINUX to permissive mode (will not persist upon reboot)

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
setenforce 0
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

centminmod pre-install custom configs

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
mkdir -p /etc/centminmod && touch /etc/centminmod/custom_config.inc
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
cat << EOF | tee /etc/centminmod/custom_config.inc
NSD_INSTALL='n' #using 3rd-party DNS service
CLANG='n'
DEVTOOLSETSIX='n'
DEVTOOLSETSEVEN='y'
DEVTOOLSETEIGHT='n'
NGINX_DEVTOOLSETGCC='y'
GENERAL_DEVTOOLSETGCC='y'
CRYPTO_DEVTOOLSET='y'

#NGX_GSPLITDWARF='y'
#PHP_GSPLITDWARF='y'

#NGXEXTRA_CCOPT=' -option'
#PHPEXTRA_CFLAGS=' -option'

LIBRESSL_SWITCH='n'

OPENSSL_TLSONETHREE='y' #TLSv1.3 control when using openssl 1.1.1-pre2
OPENSSL_VERSION='1.1.1-pre8' #https://community.centminmod.com/threads/openssl-1-1-1-first-alpha-pre-release-1-out.13996/
#TLSONETHREE='y'
TLSONETHREE_CIPHERS='TLS13-AES-128-GCM-SHA256:TLS13-AES-256-GCM-SHA384:TLS13-CHACHA20-POLY1305-SHA256:TLS13-AES-128-CCM-8-SHA256:TLS13-AES-128-CCM-SHA256:' #just prepend these into the virtual.ssl.conf file

#CLOUDFLARE_ZLIBRSET='y' #enabled by default, use CF zlib for nginx
CLOUDFLARE_ZLIBRAUTOMAX='y' #enable CF zlib lvl 9 compression for nginx

PHP_PGO='y' #use Profile Guided Optimization
#PGO option to profile live application goes here
PHPFINFO='y' #enable PHP module
CLOUDFLARE_ZLIBPHP='y' #enable use of CF zlib for PHP

PUREFTPD_DISABLED=y #disable the FTP service by default, can enable at runtime

#NGINX_UPDATEMAINTENANCE=n #see community.centminmod.com/posts/26485/
#PHP_UPDATEMAINTENANCE=n
#MARIADB_UPDATEMAINTENANCE=n

EOF
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

 

centminmod install

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
curl -O https://centminmod.com/betainstaller72.sh && chmod 0700 betainstaller72.sh && bash betainstaller72.sh
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

copy passwords and such from install output, note install times

centminmod updates \#23: \#1

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
centmin
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
cd /usr/src/local/centminmod
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

\#23: \#2

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
centmin
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

and one more time to trigger yum check upon exit, do \#24

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
centmin
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

do updates as instructed (eg. for remi repo)

change ssh port, do \#16

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
centmin
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

 

 

set up nginx vhost for default server at main hostname

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
hostnamectl status
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

verify that hostname resolves in browser

add IP to hosts file

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
vi /etc/hosts
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

append to file (replace ‘hostname’ with hostname, ‘example.com’ with domain.tld)

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
111.222.333.444 hostname.example.com hostname
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
service network restart
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

 

verify stats pages in browser, at (where \${N} is replaced with unique numbers
generated at install, see install logs)

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
http://srvr1.tivism.com/${N}_opcache.php
http://srvr1.tivism.com/memcache_${N}.php
http://srvr1.tivism.com/${N}_phpi.php
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

 

update centminmod custom configs

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
cat << EOF | tee /etc/centminmod/custom_config.inc
NSD_INSTALL='n' #using 3rd-party DNS service
CLANG='n'
DEVTOOLSETSIX='n'
DEVTOOLSETSEVEN='y'
DEVTOOLSETEIGHT='n'
NGINX_DEVTOOLSETGCC='y'
GENERAL_DEVTOOLSETGCC='y'
CRYPTO_DEVTOOLSET='y'

#NGX_GSPLITDWARF='y'
#PHP_GSPLITDWARF='y'

#NGXEXTRA_CCOPT=' -option'
#PHPEXTRA_CFLAGS=' -option'

LIBRESSL_SWITCH='n'

OPENSSL_TLSONETHREE='y' #TLSv1.3 control when using openssl 1.1.1-pre2
OPENSSL_VERSION='1.1.1-pre2' #https://community.centminmod.com/threads/openssl-1-1-1-first-alpha-pre-release-1-out.13996/
#TLSONETHREE='y'
TLSONETHREE_CIPHERS='TLS13-AES-128-GCM-SHA256:TLS13-AES-256-GCM-SHA384:TLS13-CHACHA20-POLY1305-SHA256:TLS13-AES-128-CCM-8-SHA256:TLS13-AES-128-CCM-SHA256:' #just prepend these into the virtual.ssl.conf file

#CLOUDFLARE_ZLIBRSET='y' #enabled by default, use CF zlib for nginx
CLOUDFLARE_ZLIBRAUTOMAX='y' #enable CF zlib lvl 9 compression for nginx

PHP_PGO='y' #use Profile Guided Optimization
#PGO option to profile live application goes here
PHPFINFO='y' #enable PHP module
CLOUDFLARE_ZLIBPHP='y' #enable use of CF zlib for PHP

PUREFTPD_DISABLED=y #disable the FTP service by default, can enable at runtime

#NGINX_UPDATEMAINTENANCE=n #see community.centminmod.com/posts/26485/
#PHP_UPDATEMAINTENANCE=n
#MARIADB_UPDATEMAINTENANCE=n

ENABLE_MARIADBTENTWOUPGRADE='y'

LETSENCRYPT_DETECT='y' #enable acmetool.sh integration in automated vhost creation
KEYLENGTH='ec-256' #2048, 3072, 4096, ec-256, ec-384
DUALCERTS='y' #LetsEncrypt issue both RSA and ECDH certificates, nginx serves apprt
VHOSTCTRL_CLOUDFLAREINC='y' #enable vhost include for cloudflare.conf

CF_DNSAPI='y' #enable CloudFlare DNS API for LetsEncrypt DNS validation

EOF
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
cmupdate
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

 

setup extended CSF Firewall blocklists
[[link](https://community.centminmod.com/posts/50060/)]

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
cd /usr/local/src/centminmod/tools/
bash csf-advancetweaks.sh
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

 

enable CSF Firewall native fail2ban like support
[[link](https://community.centminmod.com/posts/62343/)]

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
csf --profile backup backup-b4-customregex

cp -a /usr/local/csf/bin/regex.custom.pm /usr/local/csf/bin/regex.custom.pm.bak

egrep 'CUSTOM1_LOG|CUSTOM2_LOG|CUSTOM3_LOG|CUSTOM4_LOG' /etc/csf/csf.conf

sed -i "s|CUSTOM1_LOG = .*|CUSTOM1_LOG = \"/home/nginx/domains/\*/log/access.log\"|" /etc/csf/csf.conf

sed -i "s|CUSTOM2_LOG = .*|CUSTOM2_LOG = \"/home/nginx/domains/\*/log/error.log\"|" /etc/csf/csf.conf

sed -i "s|CUSTOM3_LOG = .*|CUSTOM3_LOG = \"/var/log/nginx/localhost.access.log\"|" /etc/csf/csf.conf

sed -i "s|CUSTOM4_LOG = .*|CUSTOM4_LOG = \"/var/log/nginx/localhost.error.log\"|" /etc/csf/csf.conf

egrep 'CUSTOM1_LOG|CUSTOM2_LOG|CUSTOM3_LOG|CUSTOM4_LOG' /etc/csf/csf.conf

wget -O /usr/local/csf/bin/regex.custom.pm https://gist.github.com/centminmod/f5551b92b8aba768c3b4db84c57e756d/raw/regex.custom.pm

csf -ra
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

 

setup email alerts for diskalert cronjob
[[this](https://community.centminmod.com/posts/59973/)]

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
EMAIL=ops@tivism.com
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
sed -i "s|EMAIL=.*|EMAIL='$EMAIL'|" /etc/cron.daily/diskalert
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

 

csf and nginx set up for CloudFlare IPs

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
cd /usr/local/src/centminmod/tools
bash csfcf.sh auto
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

 

edit /etc/my.cnf - esp. for utf8mb4 and strict_crc32

(to see active variables, to compare two my.cnf files: this amazing
[[link](https://serverfault.com/a/732206)])

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
vi /etc/my.cnf
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

add strict_crc32 in [mysqld], also comment out (or reduce) MyISAM stuff :-/

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
innodb_checksum_algorithm = strict_crc32
innodb_log_checksum_algorithm = strict_crc32
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

install addon acmetool.sh

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
cd /usr/local/src/centminmod/addons
bash acmetool.sh acmeinstall
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

centminmod multi-core use \#17

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
centmin
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

then upgrade to mariadb 10.2, recompile nginx

 

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
nginxconf
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

uncomment lines like

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
more_set_headers "Server: niginx";
more_clear_headers "X-Powered-By";
include /usr/local/nginx/conf/cloudflare.conf
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

 

nginx VHOST process
-------------------

remember to check `/root/centminlogs/nginx_addvhost_nv.log`

set up https for hostname

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
/usr/local/src/centminmod/addons/acmetool.sh acmeupdate
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

add api key and email values at /root/.acme.sh/dnsapi/dns_cf.sh

issue the test cert
[[link](https://community.centminmod.com/threads/fqdn-ssl.15147/#post-64883)]

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
/root/.acme.sh/acme.sh --issue --ocsp-must-staple --dnssleep 42 --days 60 -d srvr1.tivism.com --dns dns_cf -k ec-256 --useragent centminmod-centos-acmesh-dnscf --log /root/centminlogs/acmetool.sh-debug-log-srvr1.tivism.com.log --log-level 2 --staging
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

issue for reals

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
/root/.acme.sh/acme.sh --force --issue --ocsp-must-staple --dnssleep 42 --days 60 -d srvr1.tivism.com --dns dns_cf -k ec-256 --useragent centminmod-centos-acmesh-dnscf --log /root/centminlogs/acmetool.sh-debug-log-srvr1.tivism.com.log --log-level 2
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

install the cert

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
mkdir -p /usr/local/nginx/conf/ssl/srvr1.tivism.com
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
/root/.acme.sh/acme.sh --installcert --ecc -d srvr1.tivism.com --certpath /usr/local/nginx/conf/ssl/srvr1.tivism.com/srvr1.tivism.com-acme.cer --keypath /usr/local/nginx/conf/ssl/srvr1.tivism.com/srvr1.tivism.com-acme.key --capath /usr/local/nginx/conf/ssl/srvr1.tivism.com/srvr1.tivism.com-acme.cer --reloadCmd /usr/bin/ngxreload --fullchainpath /usr/local/nginx/conf/ssl/srvr1.tivism.com/srvr1.tivism.com-fullchain-acme.key
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

better key exchange security

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
openssl dhparam -out /usr/local/nginx/conf/ssl/srvr1.tivism.com/dhparam.pem 2048
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

copy virtual.conf as virtual.ssl.conf

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
cp -a /usr/local/nginx/conf/conf.d/virtual.conf /usr/local/nginx/conf/conf.d/virtual.ssl.conf
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

edit virtual.ssl.conf

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
vi /usr/local/nginx/conf/conf.d/virtual.ssl.conf
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

make like this

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
server {
  listen 443 ssl http2;
  server_name srvr1.tivism.com;

  ssl_dhparam          /usr/local/nginx/conf/ssl/srvr1.tivism.com/dhparam.pem;
  ssl_certificate      /usr/local/nginx/conf/ssl/srvr1.tivism.com/srvr1.tivism.com-acme.cer;
  ssl_certificate_key  /usr/local/nginx/conf/ssl/srvr1.tivism.com/srvr1.tivism.com-acme.key;
  ssl_certificate_key  /usr/local/nginx/conf/ssl/srvr1.tivism.com/srvr1.tivism.com.key;
  include /usr/local/nginx/conf/ssl_include.conf;

  http2_max_field_size 16k;
  http2_max_header_size 32k;
  # dual cert supported ssl ciphers
  ssl_ciphers     TLS13-AES-128-GCM-SHA256:TLS13-AES-256-GCM-SHA384:TLS13-CHACHA20-POLY1305-SHA256:TLS13-AES-128-CCM-8-SHA256:TLS13-AES-128-CCM-SHA256:EECDH+CHACHA20-draft:EECDH+CHACHA20:EECDH+ECDSA+AESGCM:EECDH+aRSA+AESGCM:EECDH+ECDSA+SHA256:EECDH+ECDSA+SHA384:EECDH+aRSA+SHA256:EECDH+aRSA+SHA384:EECDH+AES128:!aNULL:!eNULL:!LOW:!3DES:!MD5:!EXP:!PSK:!SRP:!DSS:!RC4:!EDH-DSS-DES-CBC3-SHA:!EDH-RSA-DES-CBC3-SHA:!KRB5-DES-CBC3-SHA:!CAMELLIA;
  ssl_prefer_server_ciphers   on;
  #add_header Alternate-Protocol  443:npn-spdy/3;
  #add_header Strict-Transport-Security "max-age=31536000; includeSubdomains;";
  #add_header X-Frame-Options SAMEORIGIN;
  #add_header X-Xss-Protection "1; mode=block" always;
  #add_header X-Content-Type-Options "nosniff" always;
  #spdy_headers_comp 5;
  ssl_buffer_size 1369;
  ssl_session_tickets on;

  # enable ocsp stapling
  resolver 8.8.8.8 8.8.4.4 valid=10m;
  resolver_timeout 10s;
  ssl_stapling on;
  ssl_stapling_verify on;
  ssl_trusted_certificate
  /usr/local/nginx/conf/ssl/srvr1.tivism.com/srvr1.tivism.com-acme.cer;

        root   html;
        access_log              /var/log/nginx/localhost.access.log     combined buffer=8k flush=1m;
        error_log               /var/log/nginx/localhost.error.log      error;

# ngx_pagespeed & ngx_pagespeed handler
#include /usr/local/nginx/conf/pagespeed.conf;
#include /usr/local/nginx/conf/pagespeedhandler.conf;
#include /usr/local/nginx/conf/pagespeedstatslog.conf;

# limit_conn limit_per_ip 16;
# ssi  on;

        location /nginx_status {
        stub_status on;
        access_log   off;
        allow 127.0.0.1;
        #allow youripaddress;
        deny all;
        }

            location / {

# block common exploits, sql injections etc
#include /usr/local/nginx/conf/block.conf;

#Enables directory listings when index file not found
#autoindex  on;
          
            }

include /usr/local/nginx/conf/staticfiles.conf;
include /usr/local/nginx/conf/include_opcache.conf;
include /usr/local/nginx/conf/php.conf;
#include /usr/local/nginx/conf/phpstatus.conf;
include /usr/local/nginx/conf/drop.conf;
#include /usr/local/nginx/conf/errorpage.conf;
#include /usr/local/nginx/conf/vts_mainserver.conf;

       }
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

restart nginx, test https

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
ngxrestart
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

create redirect to https: first, move virtual.conf

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
mv /usr/local/nginx/conf/conf.d/virtual.conf /usr/local/nginx/conf/conf.d/virtual.conf-disabled
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

then edit virtual.ssl.conf

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
vi /usr/local/nginx/conf/conf.d/virtual.ssl.conf
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

above existing, add new server block like

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
server {
   listen 80;
   server_name srvr1.tivism.com;
   return 301 https://srvr1.tivism.com$request_uri;
 }
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

edit existing server block to be like

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
listen 443 default_server ssl http2 fastopen=256 backlog=2048 reuseport;
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
ngxrestart
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

test https and redirect, then to make things nice for centminmod

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
cp /usr/local/nginx/conf/conf.d/virtual.ssl.conf /usr/local/nginx/conf/conf.d/virtual.ssl.conf-bak

mv /usr/local/nginx/conf/conf.d/virtual.ssl.conf /usr/local/nginx/conf/conf.d/virtual.conf
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
ngxrestart
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

drop TLS 1.0

find where ssl_protocols is being set (note: can use this approach to find lots
of info --\> combined nginx conf, into a file for review)

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
nginx -T |&tee -a ~/combined_nginxconf.txt
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

examine output file and see ssl_protocols set in
/usr/local/nginx/conf/ssl_include.conf

edit ssl_include.conf so that ssl_protocols is like

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
ssl_protocols TLSv1.1 TLSv1.2 TLSv1.3;
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
ngxrestart
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

upload a favicon.ico to /usr/local/nginx/html/favicon.ico  
test via webpagetest.org and ssllabs.com (use latest chrome devtools to see
TLSv1.3 working)  
smile :)

 

if using phpmyadmin.sh addon tool, see this to resolve conflict
[[link](https://community.centminmod.com/threads/fqdn-ssl.15147/#post-64928)]

or don’t use phpmyadmin, instead use HeidiSQL
[[link](https://community.centminmod.com/threads/guide-how-to-access-your-mysql-databases-using-heidisql-instead-of-phpmyadmin.11786/)]
(may need to make ssh private key without passphrase)

or try
[TOAD](http://community-downloads.quest.com/toadsoft/MySQL/ToadforMySQL_Freeware_8.0.0.296.zip)
or [similar](https://dev.mysql.com/downloads/workbench/) via ssh tunnel (windows
10 users see
[this](https://support.cs.wwu.edu/index.php/Tunneling_MySQL_ports_through_SSH#Setting_up_an_SSH_tunnel_with_PuTTY_in_Windows)
or just use plink)

 

new site vhost creation
-----------------------

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
nv -d tst0.xyz -s lelived
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

ERROR nginx does not restart after vhost creation (before LE)

“...duplicate listen options for 0.0.0.0:443” ...now, I did so some slightly
funky stuff in messing with the default virtual.conf ...however, it seems like
this is an issue with reuseport (when at other than :80)... removing reuseport
from tst0.xyz.ssl.conf makes it work (oddly, removing reuseport from
virtual.conf does not make it work)... so, at this point all new vhosts created
with `nv` seem to be breaking things --\> removing the reuseport value seems to
fix things...

 

 

 

 

 

================notes only below here=====================

https://centminmod.com/vhost.php

https://community.centminmod.com/threads/add-nginx-vhost-via-ssh-command-line-via-nv-sh-usr-bin-nv.3616/

 

https://centminmod.com/letsencrypt-acmetool-https.html

https://community.centminmod.com/threads/official-acmetool-sh-testing-thread-for-centmin-mod-123-09beta01.8290/

https://community.centminmod.com/threads/acmetool-sh-fix-dual-cert-mode-ssl_trusted_certificate-concat.14626/\#post-62761

 

 

TODO
----

 

### NGINX - esp re how best to config for multisite

 

https://stackoverflow.com/a/21165728

 

 

 

 

More TODO
---------

https://community.centminmod.com/threads/automatic-nightly-yum-updates-with-yum-cron.1507/

https://community.centminmod.com/threads/maldet-linux-malware-detect-addon-discussion.846/

https://community.centminmod.com/threads/centmin-mod-auditd-support-added-in-latest-123-09beta01.9071/

 

https://centminmod.com/addons.html\#wpcli

https://centos.pkgs.org/7/ius-x86_64/php72u-cli-7.2.7-2.ius.centos7.x86_64.rpm.html

 

acmetool.sh

 

https://community.centminmod.com/threads/cloudflare-custom-nginx-logging.14790/

 

https://community.centminmod.com/threads/setting-up-cloudflare-authenticated-origin-pulls-protecting-your-origins.13847/

https://community.centminmod.com/threads/improving-cloudflare-connections-to-origin-server-use-ecdsa-ssl-certs.14817/

 

centminmod recompile PHP using production profile

 

modify tuned-adm profile

https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/performance_tuning_guide/sect-red_hat_enterprise_linux-performance_tuning_guide-performance_monitoring_tools-tuned_and_tuned_adm

 

https://community.centminmod.com/threads/how-to-install-redis-server-on-centmin-mod-lemp-stack.4546/\#post-18828

 

https://centminmod.com/phpfpm.html\#customphpini

 

https://community.centminmod.com/threads/wp-super-cache-keycdn-cache-enabler-redis-cache-user-agent-bypass-for-wordpress.5472/

 

https://community.centminmod.com/threads/csfcf-sh-automate-cloudflare-nginx-csf-firewall-setups.6241/\#post-62785

 

https://community.centminmod.com/threads/123-09beta01-updated-nginx-zlib-routine-with-optional-cloudflare-zlib-support.13521/\#post-58625

https://community.centminmod.com/threads/how-to-boost-centmin-mod-lemp-stack-performance.13776/
